import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

enum AppNotificationChannelType {
  test,
}

class LocalNotificationService {
  static final FlutterLocalNotificationsPlugin notificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static void initialize() {
    var initializationSettingsAndroid =
        const AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = DarwinInitializationSettings(
      onDidReceiveLocalNotification: (id, title, body, payload) {
        debugPrint('payload: $payload');
        if (payload != null || payload != '') {
          debugPrint('notification payload: $payload');
          /*Navigator.pushNamed(context, payload);*/
        }
      },
    );

    notificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.requestPermission();

    var initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );
    notificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: (details) {
        onSelectNotification(details);
      },
      /*onDidReceiveBackgroundNotificationResponse: (details) {
        debugPrint('payload: ${details.payload}');
        if (details.payload != null || details.payload != '') {
          debugPrint('notification payload: ${details.payload}');

          */ /*Navigator.pushNamed(context, payload);*/ /*
        }
      },*/
    );
  }

  static Future showNotification({
    String? title,
    String? body,
    String? payload,
    String? imageUrl,
    required AppNotificationChannelType appNotificationChannelType,
  }) async {
    debugPrint('show notification: $title');
    try {
      int id = DateTime.now().millisecondsSinceEpoch ~/ 1000;
      BigPictureStyleInformation? bigPictureStyleInformation;

      AndroidNotificationDetails androidPlatformChannelSpecifics =
          AndroidNotificationDetails(
        'test',
        'test channel.',
        channelDescription: 'This is our channel.',
        importance: Importance.max,
        priority: Priority.high,
        styleInformation: bigPictureStyleInformation,
      );

      var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
      );
      await notificationsPlugin.show(
        id,
        title,
        body,
        platformChannelSpecifics,
        payload: payload,
      );
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  static void onSelectNotification(
    NotificationResponse response,
  ) async {
    debugPrint('payload: ${response.payload}');
    if (response.payload != null || response.payload != '') {
      debugPrint('notification payload: ${response.payload}');
      /*Navigator.pushNamed(context, payload);*/
    }
  }
}
